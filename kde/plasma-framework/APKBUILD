# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: Bart Ribbers <bribbers@disroot.org>
pkgname=plasma-framework
pkgver=5.51.0
pkgrel=0
pkgdesc="Plasma library and runtime components based upon KF5 and Qt5"
arch="all"
url="https://community.kde.org/Frameworks"
license="GPL-2.0"
depends=""
depends_dev="kdoctools-dev kactivities-dev kwindowsystem-dev ki18n-dev kiconthemes-dev kpackage-dev
			 kdeclarative-dev knotifications-dev qt5-qtdeclarative-dev qt5-qtsvg-dev kio-dev
			 kwayland-dev kdbusaddons-dev qt5-qtx11extras-dev karchive-dev kguiaddons-dev
			 kservice-dev kbookmarks-dev kcompletion-dev kitemviews-dev kjobwidgets-dev
			 solid-dev kxmlgui-dev kglobalaccel-dev kconfig-dev kconfigwidgets-dev kauth-dev
			 kcoreaddons-dev kcodecs-dev kwidgetsaddons-dev kirigami2-dev"
makedepends="$depends_dev extra-cmake-modules qt5-qttools-dev doxygen"
source="https://download.kde.org/stable/frameworks/${pkgver%.*}/$pkgname-$pkgver.tar.xz"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
options="!check" # Fails due to requiring running X11

build() {
	cmake \
		-DCMAKE_BUILD_TYPE=Release \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DKDE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	make
}

check() {
	cd "$builddir"
	CTEST_OUTPUT_ON_FAILURE=TRUE ctest
}

package() {
	cd "$builddir"
	make DESTDIR="${pkgdir}" install
}
sha512sums="5c5949101ce9f3c520f9b2663041292b4f070fc97061678e8aa8ea05b677f15ff5804ad66242cc3ffb11dd32ced50fdfa976c1f36540f0f09dfd6e5df114d800  plasma-framework-5.51.0.tar.xz"
